var mongoose = require('mongoose');
var Schema = mongoose.Schema;
module.exports = mongoose.model('cheque', {
    issuer: String,
    payee: String,
    date: String,
    signature: String,
    amount: String,
    amountText:String,
    hash: String,
    key: String,
    accountPayee:String,
});


