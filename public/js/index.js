$(document).ready(function() {
  $("[name='sign']").change(function() {
    fillFileToImg(this,$("#signPreview img"));
  });
  $("#chainChequeForm").validate({
    rules: {

    },
    ignore: "[type='file']", // this is necessary since our radio checkboxes are hidden.
    invalidHandler: function (form, validator) {
      thisForm = form.currentTarget;
      errors = validator.numberOfInvalids();
      if (errors) {
        var ele = $(validator.errorList[0].element);
        if(ele.attr("name")=="sign") {
          alert(validator.errorList[0].message);
        }
        validator.errorList[0].element.focus();
      }
    },
    errorPlacement: function (error, element) {
      return;
    },
    submitHandler: function (form) {
      if ($("[name='sign']").val() == "") {
        return;
      }
      $(form).ajaxSubmit(function(resp) {
        if(resp && resp.status=="1") {
          if(resp.pdflink) {
            var fileName = resp.pdflink.split("/");
            fileName = fileName[fileName.length - 1];
            $("#successMsg a").attr({ "href": resp.pdflink, "download": "eCheque.pdf" });
            $("#successMsg").show();
            // SaveToDisk(resp.pdflink,fileName);
            // alert("You can download the file from : "+resp.pdflink);
          }
          $(form).resetForm();
        } else {
          console.log(resp);
          alert(resp.message || resp.msg || 'Unable to process');
        }
      });
    },
    messages: {
      "sign":"Please upload a valid jpg/jpeg/png signature image.",
      "amount":"Please enter a valid amount in numbers",
      "date":"Please enter date",
      "email":"Please enter a valid email id"
    }
  });
  $("#chainChequeForm").on('reset',function() {
    $("#signPreview img").attr("src","");
    // $("#successMsg").hide();
  });
});



function fillFileToImg(input, destObj) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      destObj.attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}
function numToString(x) {
  var r = 0;
  var txter = x;
  var sizer = txter.length;
  var numStr = "";
  if (isNaN(txter)) {
    return " Invalid number";
  }
  var n = parseInt(x);
  var places = 0;
  var str = "";
  var entry = 0;
  while (n >= 1) {
    r = parseInt(n % 10);

    if (places < 3 && entry == 0) {
            numStr = txter.substring(txter.length - 0, txter.length - 3) // Checks for 1 to 999.
            str = onlyDigit(numStr); //Calls function for last 3 digits of the value.
            entry = 1;
          }

          if (places == 3) {
            numStr = txter.substring(txter.length - 5, txter.length - 3)
            if (numStr != "") {
              str = onlyDigit(numStr) + " Thousand " + str;
            }
          }

          if (places == 5) {
            numStr = txter.substring(txter.length - 7, txter.length - 5) //Substring for 5 place to 7 place of the string
            if (numStr != "") {
                str = onlyDigit(numStr) + " Lakhs " + str; //Appends the word lakhs to it
              }
            }

            if (places == 6) {
            numStr = txter.substring(txter.length - 9, txter.length - 7)  //Substring for 7 place to 8 place of the string
            if (numStr != "") {
                str = onlyDigit(numStr) + " Crores " + str;        //Appends the word Crores
              }
            }

            n = parseInt(n / 10);
            places++;
          }
          return (str);
        }
        function onlyDigit(n) {
    //Arrays to store the string equivalent of the number to convert in words
    var units = ['', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
    var randomer = ['', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
    var tens = ['', 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
    var r = 0;
    var num = parseInt(n);
    var str = "";
    var pl = "";
    var tenser = "";
    while (num >= 1) {
      r = parseInt(num % 10);
      tenser = r + tenser;
        if (tenser <= 19 && tenser > 10) //Logic for 10 to 19 numbers
        {
          str = randomer[tenser - 10];
        }
        else {
            if (pl == 0)        //If units place then call units array.
            {
              str = units[r];
            }
            else if (pl == 1)    //If tens place then call tens array.
            {
              str = tens[r] + " " + str;
            }
          }
        if (pl == 2)        //If hundreds place then call units array.
        {
          str = units[r] + " Hundred " + str;
        }

        num = parseInt(num / 10);
        pl++;
      }
      return str;
    }

  function SaveToDisk(fileURL, fileName) {
    // for non-IE
    if (!window.ActiveXObject) {
        var save = document.createElement('a');
        save.href = fileURL;
        save.target = '_blank';
        save.download = fileName || 'unknown';

        var evt = new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': false
        });
        save.dispatchEvent(evt);

        (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }

    // for IE < 11
    else if ( !! window.ActiveXObject && document.execCommand)     {
        var _window = window.open(fileURL, '_blank');
        _window.document.close();
        _window.document.execCommand('SaveAs', true, fileName || fileURL)
        _window.close();
    }
}