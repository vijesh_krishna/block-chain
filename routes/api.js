var express = require('express');
var router = express.Router();
var servertools = require.main.require('./servertools.js');
var cheques = require.main.require('./models/cheque.js');

var app_Secret = '9293a38d-eacb-4c93-b3d0-8e6d6bb8ede6';
const Stampery = require('stampery');

var stampery = new Stampery(app_Secret);
var PDFDocument = require('pdfkit');
var blobStream = require('blob-stream');
var QRCode = require('qrcode');
var async = require('async');




var fs = require('fs-extra');

router.post('/addEcheque', function (req, res) {
    var resp = { "status": "0", "msg": "Could not add echeque to block chain", "url": "" };
    var inputCheque = req.body.issuer + "," + req.body.payee + "," + req.body.amount + "," + req.body.date;
    var h = stampery.hash(inputCheque);
    stampery.stamp(h, function (err, stamp) {
        if (err) {
           console.error(err);
            res.send(resp);
            res.end();
            return
        }
        else {

            var amtToText;
            var signFilePath;
            if (req.files && req.files.sign) {
                signFilePath = req.files.sign.name;
            }
            if(req.body.amount){
                amtToText=servertools.numToString(req.body.amount)+" only";
            }
           
            var saveCheque = new cheques({
                issuer: req.body.issuer,
                payee: req.body.payee,
                date: req.body.date,
                signature: signFilePath,
                amount: req.body.amount,
                amountText:amtToText,
                hash: stamp.hash,
                key: stamp.id,
                accountPayee: req.body.accountPayee
            });
            var qrCodeFileName = new Date().valueOf() + ".png";
            async.series([
                function (callback) {
                    QRCode.toFile('public/uploads/qrcode/' + qrCodeFileName, saveCheque.key, {
                        color: {
                            dark: '#00F',  // Blue dots
                            light: '#0000' // Transparent background
                        }
                    }, function (err) {
                        if (err) {
                            console.log("Err generating qrcode", err);
                        }
                        return callback();
                    });
                }
            ], function (err) {
                saveCheque.save(function (err, savedDoc) {
                    if (err) {
                        console.log("Cheque could not be saved err" + err);
                        resp.msg = "Unable to save";
                        res.json(resp);
                        return;
                    } else {
                        
                        var doc = new PDFDocument();
                        var stream = doc.pipe(blobStream());

                        var values = {
                            'date': saveCheque.date,
                            'name': saveCheque.payee,
                            'amount': saveCheque.amount,
                            'text': saveCheque.amountText,
                            'curreny': 'Rupees',
                            'currenyText': 'Rs',
                            'sign': 'public/uploads/' + signFilePath,
                            'qrimage': 'public/uploads/qrcode/' + qrCodeFileName,
                        };



                        var borderEnd = 500;
                        var borderStart = 50;

                        //placing qr code
                        doc.image(values.qrimage, borderStart + 50, 30, { fit: [70, 70] });

                        doc.fontSize(14);


                        //adding date
                        var dateInfo = {
                            'start': { 'x': 300, y: 50 }, 'gap': 20
                        }
                        doc.text('Date', dateInfo.start.x, dateInfo.start.y);
                        doc.text(values.date, dateInfo.start.x + 50, dateInfo.start.y);
                        doc.lineCap('butt').moveTo(dateInfo.start.x, dateInfo.start.y + dateInfo.gap).lineTo(borderEnd, dateInfo.start.y + dateInfo.gap).stroke()

                        //adding payee
                        var payeeInfo = {
                            'start': { 'x': borderStart, y: dateInfo.start.y + 50 }, 'gap': 40, 'width': 100
                        }
                        doc.text('Pay Against', payeeInfo.start.x, payeeInfo.start.y);
                        doc.text('this cheque', payeeInfo.start.x, (payeeInfo.start.y + payeeInfo.gap / 2));
                        doc.text(values.name, payeeInfo.start.x + payeeInfo.width, payeeInfo.start.y + (payeeInfo.gap / 2));
                        doc.lineCap('butt').moveTo(payeeInfo.start.x, payeeInfo.start.y + payeeInfo.gap).lineTo(borderEnd, payeeInfo.start.y + payeeInfo.gap).stroke()

                        //adding amount text
                        var amtTxtInfo = {
                            'start': { 'x': borderStart, y: payeeInfo.start.y + 60 }, 'gap': 40, 'width': 100, 'end': 350
                        }
                        doc.text('On bearer', amtTxtInfo.start.x, amtTxtInfo.start.y);
                        doc.text(values.curreny, amtTxtInfo.start.x, (amtTxtInfo.start.y + amtTxtInfo.gap / 2));
                        doc.text(values.text, amtTxtInfo.start.x + amtTxtInfo.width, amtTxtInfo.start.y + (amtTxtInfo.gap / 2));
                        doc.lineCap('butt').moveTo(amtTxtInfo.start.x, amtTxtInfo.start.y + amtTxtInfo.gap).lineTo(amtTxtInfo.end, amtTxtInfo.start.y + amtTxtInfo.gap).stroke()

                        //adding amount box
                        var amtInfo = {

                        }
                        doc.text(values.currenyText, (amtTxtInfo.start.x + amtTxtInfo.end - 15), amtTxtInfo.start.y + (amtTxtInfo.gap / 2));
                        doc.text(values.amount, (amtTxtInfo.start.x + amtTxtInfo.end + 25), amtTxtInfo.start.y + (amtTxtInfo.gap / 2));
                        doc.lineCap('square').rect(amtTxtInfo.end + 25, amtTxtInfo.start.y, 125, 40).stroke();

                        //signature text
                        var signInfo = {
                            'start': { x: borderStart, y: amtTxtInfo.start.y + 75 }, 'xGap': 150, 'gap': 20
                        }

                        doc.text('Signature', signInfo.start.x + signInfo.xGap, signInfo.start.y);
                        doc.lineCap('butt').
                            moveTo(signInfo.start.x + signInfo.xGap, signInfo.start.y + signInfo.gap).lineTo(borderEnd, signInfo.start.y + signInfo.gap).stroke();

                        //doc.rect(20, 25, 575, doc.y+10)..fillOpacity(0.5).fillAndStroke('gray')

                        //adding signature image

                        //Fit the image within the dimensions
                        if(values.sign){
                            doc.image(values.sign, signInfo.start.x + signInfo.xGap + 90, signInfo.start.y - 15, { fit: [200, 30] });
                        }
                       
                        // Stream contents to a file
                        var pdfFileName = new Date().valueOf() + ".pdf";
                        doc.pipe(fs.createWriteStream('./public/uploads/' + pdfFileName,{'flags':'w+'})).on('finish', function () {
                            console.log('PDF closed');
                        });

                        // Close PDF and write file.
                        doc.end();

                        // doc.pipe(res);

                        resp.status = "1";
                        resp.pdflink ="http://"+req.hostname + ":5700/uploads/" + pdfFileName;
                        resp.msg = "Echeque generated.Sucessfully added to block chain";
                        res.json(resp);
                        if(req.body.fromWeb && req.body.email) {
                            var mailInfo ={
                                to: req.body.email,
                                subject: 'Your eCheque',
                                text: '',
                                html: '<p>Echeque generated.Sucessfully added to block chain.Please click below link.</p><p><a href="'+resp.pdflink+'">Get eCheque</a></p>'
                            }
                            servertools.sendMail(mailInfo);
                        }
                        return;
                    }
                });
            });


        }

    });

    // res.send('respond with a resource');


});

router.post('/getByHash', function (req, res) {


    stampery.getByHash(req.body.hashKey, function (err, res) {
        if (err) {
            return console.error(err);
        }
        return console.log(res);
    });

});


router.post('/getById', function (req, res) {

    var resp = { "status": "0", "msg": "Could not find echeque in block chain", "chequeDetails": {}, "receipts": {} };
    stampery.getById(req.body.keyId, function (err, stampRes) {
        if (err) {
            console.error(err);
            res.json(resp);
            res.end();

        }
        if (stampRes) {
            if (stampery.prove(stampRes.receipts)) {
                cheques.find({ 'key': req.body.keyId }, function (err, chequeFound) {
                    if (err) {
                        resp.msg = "error While finding E Cheque";
                        res.json(resp);
                        res.end();

                    }
                    else if (chequeFound.length) {

                        resp.status = "1";
                        resp.msg = "Cheque is valid";

                        var chequeObj = {};
                        chequeObj = chequeFound[0];

                        resp['chequeDetails'] = chequeObj;
                        resp['receipts'] = stampRes.receipts;

                        console.log(resp);
                        res.json(resp);
                        res.end();


                    }
                    else {
                        resp.msg = "ECheque not found in db";
                        res.json(resp);
                        res.end();
                        return;
                    }

                });
            }
            else {

                res.json(resp);
                res.end();
                return;

            }
            // return console.log('Valid: ', stampery.prove(res.receipts));
        }
        else {
            res.end();
            return;
        }
    });
});


module.exports = router;