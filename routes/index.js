﻿var express = require('express');
var router = express.Router();
var servertools = require('../servertools.js');
var passport = require('passport');
var PDFDocument = require('pdfkit');                      
var blobStream  = require ('blob-stream');

var fs=require('fs');


router.get('/',function(req,res) {
  res.render('index');
});


/* GET home page. */
router.get('/', function (req, res) {
    // create a document and pipe to a blob
var doc = new PDFDocument();
var stream = doc.pipe(blobStream());


lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in suscipit purus.  Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus nec hendrerit felis. Morbi aliquam facilisis risus eu lacinia. Sed eu leo in turpis fringilla hendrerit. Ut nec accumsan nisl.'

doc.fontSize(8);
doc.text('This text is left aligned. ' + lorem,0,100, {
  width: 410,
  align: 'left'});

doc.moveDown();
doc.text('This text is centered. ' + lorem,{
  width: 410,
  align: 'center'
});

doc.moveDown();
doc.text('This text is right aligned. ' + lorem, {
  width: 410,
  align: 'right'
});
doc.moveDown();
doc.text('This text is justified. ' + lorem, {
  width: 410,
  align: 'justify'
});
doc.rect(doc.x, 0, 410, doc.y).stroke()

doc.end();
doc.pipe( res );

});
router.get('/login', function (req, res) {
    res.render('login', { pageTitle: 'Login' });
});

router.post('/login', function (req, res, next) {
    passport.authenticate('login', function (err, user, info) {
        if (err) { return next(err); }
        if (!user) { return res.redirect('/login'); }
        req.logIn(user, function (err) {
            if (err) { return next(err); }
            if (req.session.lasturl != null)
                return res.redirect(req.session.lasturl);
            else
                return res.redirect('/');
        });
    })(req, res, next);
});

//sigup page

router.get('/signup', function (req, res) {
    res.render('signup', { pageTitle: 'Signup' });
});

router.post('/signup', passport.authenticate('signup', {
    successRedirect: '/',
    failureRedirect: '/signup',
    failureFlash : true
}));

router.get("/logout", function (req, res) {
    req.logout();
    res.redirect("/");
});
module.exports = router;