﻿var express = require('express');
var router = express.Router();
var servertools = require('../servertools.js');
var passport = require('passport');
var PDFDocument = require('pdfkit');                      
var blobStream  = require ('blob-stream');

var fs=require('fs');



/* GET home page. */
router.get('/', function (req, res) {
    // create a document and pipe to a blob
var doc = new PDFDocument();
var stream = doc.pipe(blobStream());

// draw some text
doc.moveTo(300, 75)
   .lineTo(373, 301)
   .lineTo(181, 161)
   .lineTo(419, 161)
   .lineTo(227, 301)
   .fill('red', 'even-odd');  

var loremIpsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam in...';  

doc.y = 320;
doc.fillColor('black')
doc.text(loremIpsum, {
   paragraphGap: 10,
   indent: 20,
   align: 'justify',
   columns: 2
});  
doc.end();
doc.pipe( res );

});
router.get('/login', function (req, res) {
    res.render('login', { pageTitle: 'Login' });
});

router.post('/login', function (req, res, next) {
    passport.authenticate('login', function (err, user, info) {
        if (err) { return next(err); }
        if (!user) { return res.redirect('/login'); }
        req.logIn(user, function (err) {
            if (err) { return next(err); }
            if (req.session.lasturl != null)
                return res.redirect(req.session.lasturl);
            else
                return res.redirect('/');
        });
    })(req, res, next);
});

//sigup page

router.get('/signup', function (req, res) {
    res.render('signup', { pageTitle: 'Signup' });
});

router.post('/signup', passport.authenticate('signup', {
    successRedirect: '/',
    failureRedirect: '/signup',
    failureFlash : true
}));

router.get("/logout", function (req, res) {
    req.logout();
    res.redirect("/");
});
module.exports = router;