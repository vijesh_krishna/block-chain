﻿var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multer = require('multer');
var swig = require('swig');
var routes = require('./routes/index');

var flash = require('connect-flash');
var api=require('./routes/api.js');

var app = express();

// view engine setup
app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
var mime = require('mime-types');
app.use(multer({
    dest: './public/uploads/',
    onFileUploadStart: function (file) {
        // keep input name as key and allowed mimetypes in arr
        //console.log(JSON.stringify(file));
        var filesAllowedSet = {
            "sign": ['image/png', 'image/jpg', 'image/jpeg'],
        };
        var lookup = mime.lookup(file.path);
        console.log(lookup);
        if (file && file.fieldname) {
            if (filesAllowedSet[file.fieldname]) {
                var tmpMimeType=file.mimetype;
                if (file.fieldname == 'sign' && filesAllowedSet[file.fieldname].indexOf(file.mimetype) == -1) {
                    tmpMimeType = lookup;
                }
                return filesAllowedSet[file.fieldname].indexOf(tmpMimeType) != -1;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },
}));
app.use(express.static(path.join(__dirname, 'public')));


app.use(flash());

// -------------Passport & mongoose Code-------------
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/db-name');

var passport = require('passport');
var expressSession = require('express-session');
app.use(expressSession({
    secret: 'project#sec', saveUninitialized: true,
    resave: true , cookie: { maxAge: 7200000, httpOnly: false }, rolling: true
}));
app.use(passport.initialize());
app.use(passport.session());

// Initialize Passport
// var initPassport = require('./passport/init');
// initPassport(passport);
//-------------Passpord & mongoose code ends-------------
var randomNumber = Math.random() * 10000; //To prevent caching
app.use(function (req, res, next) {
    res.locals = {
        isAuth: req.isAuthenticated(),
        "messages": req.flash('warning'),
        'user': req.user,
        url: req.path,
        'randomNumber':randomNumber,
    };
    next();
});



app.use('/', routes);
app.use('/api',api);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.set('port', process.env.PORT || 5700);

var server = app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + server.address().port);
});


module.exports = app;
